import logo from './logo.svg';
import './App.css';

import Header from './components/Header';
import AboutUsPages from './pages/AboutUsPage';
import GradePages from './pages/GradePage';

import {Routes,Route} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Header />
        <Routes>

        <Route path="about" element={
                  <AboutUsPages/>
                  }/>
          <Route path="/" element={
            <GradePages/>
            }/>
         
          
      
        </Routes>
    </div>
  );
}

export default App;
