import Grade from "../components/Grade";
import { useState, useEffect } from "react";

import Button from '@mui/material/Button';
import Grid  from '@mui/material/Grid';
import { Typography, Box, Container } from "@mui/material";

function GradePage() {

       const[name,setName] = useState("")
       const[score,setScore] = useState("")
       const[transgrade,setTrangrade] = useState("")
       const[grade,setGrade] = useState("")

function GPA() {
          let s = parseInt(score)
           let grade = s
          setGrade(grade);
             if(grade >= 80)
                {
                    setTrangrade("A")
                }
             else if(grade >= 75)
                {
                    setTrangrade("B+")
                }
            else if (grade >= 70)
                {
                    setTrangrade("B")
                }
            else if(grade >= 65)
                {
                    setTrangrade("C+")
                }
            else if(grade >= 60)
                {
                    setTrangrade("C")
                }
            else if(grade >= 55)
                {
                    setTrangrade("D+")
                }
            else if(grade >= 50)
                {
                    setTrangrade("D")
                }
            else if(grade >= 45)
                {
                    setTrangrade("E")
                }


}
return (
    <Container maxWidth='lg'>    
    <Grid container spacing={2} sx={{ marginTop : "10px"}}>
        
        <Grid item xs={12}>
        <Typography variant="h6">
            ยินดีต้้อนรับสู่เว็บคำนวณ Grade
            </Typography>
                </Grid>
                <Grid item xs={8}>
                    <Box sx={{ textAlign: 'left'}}>
                          Name: <input type = "text"
                              value={name}
                              onChange={ (e) => { setName(e.target.value) } } /> 
                              <br /><br /><br />
                          Score: <input type = "text" 
                              value={score}
                              onChange={ (e) => { setScore(e.target.value) } } /> 

                            <br /><br /><br />

                            <Button variant="contained" onClick={ ()=>{ GPA () } }> แสดงผล 
                            </Button>
                       </Box>
                </Grid>
                <div>
                   
                 ผลการคำนวณเกรดเฉลี่ยรายวิชา 344-243
                    <Grade 
                        name={ name }
                        score = { score }
                        Grade = { transgrade }
                    />
                </div>
         
    </Grid>
</Container>

    );
}


export default GradePage;
